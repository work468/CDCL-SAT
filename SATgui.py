#!/usr/bin/env python3

import sys
import tkinter as tk
from tkinter import filedialog
import SATsolver

class SATgui:

    def __init__(self, debug_mode = False):
        self.SAT = SATsolver.SATsolver(debug_mode)
        self.setup_gui()
        self.setup_functionality()
        self.window.mainloop()

    def get_input(self):
        input = []
        if self.read_text.get():
            text_input = self.text_entry.get("1.0", tk.END)
            text_input = text_input.split('\n')
            input += text_input

        return input

    def solve(self, sat_algorithm):
        input = self.get_input()

        results = []
        for problem in input:
            if problem == '':
                continue

            parsed = self.SAT.parseCNF(problem)
            if parsed == []:
                results += ([str(problem) + ' -> Invalid problem (not in CNF)'])
                continue

            result = sat_algorithm(parsed)
            if not self.SAT.assignment_satisfies(parsed, result):
                print('Non-satisfying return for problem ' + str(problem) + ' (ignore if unsatisfiable)')
            if result ==[]:
                results += ([str(problem) + ' -> No solutions'])
            else:
                solution = ''
                for assignment in result:
                    solution += str(assignment[0]) + ': ' + str(assignment[1]) + '   '
                results += ([str(problem) + ' -> ' + solution])

        if not self.file_input_parsed == [] and self.read_file.get():
            result = sat_algorithm(self.file_input_parsed)
            if result ==[]:
                results += (['File DIMACS problem -> No solutions'])
            else:
                solution = ''
                for assignment in result:
                    solution += str(assignment[0]) + ': ' + str(assignment[1]) + '   '
                results += (['File DIMACS problem -> ' + solution])
        label = ''
        for result in results:
            label += result + '\n'

        self.results_label.config(text=label)

    def select_file(self, event):
        self.filename = filedialog.askopenfilename()
        self.selected_file_label.config(text=self.filename)
        with open(self.filename, 'r') as file_input:
            file_input = file_input.read().split('\n')
            self.file_input_parsed = self.SAT.parse_dimacs(file_input)

    def solve_cdcl(self, event):
        self.solve(self.SAT.cdcl_parsed)

    def solve_breadth_first(self, event):
        self.solve(self.SAT.breadth_first_sat_parsed)

    def solve_dpll(self, event):
        self.solve(self.SAT.sat_dpll_parsed)

    def quick_solve(self, event):
        if self.file_input_parsed == []:
            self.results_label.config(text='No file selected')
            return

        result = self.SAT.portfolio(self.filename)[0]
        label = ''

        if result == []:
            label = (['File DIMACS problem -> No solutions'])
        else:
            solution = ''
            for assignment in result:
                solution += str(assignment[0]) + ': ' + str(assignment[1]) + '   '
            label = ('File DIMACS problem -> ' + solution)

        self.results_label.config(text=label)

    def setup_functionality(self):
        self.file_select_button.bind("<ButtonRelease>", self.select_file)
        self.file_input_parsed = []
        self.dpll_button.bind("<ButtonRelease>", self.solve_dpll)
        self.cdcl_button.bind("<ButtonRelease>", self.solve_cdcl)
        #self.breadth_first_button.bind("<ButtonRelease>", self.solve_breadth_first)
        self.quick_solve_button.bind("<ButtonRelease>", self.quick_solve)

    def setup_gui(self):
        self.window = tk.Tk()
        self.window.resizable(False, False)
        self.window.title("SAT Solver")

        self.problem_select_frame = tk.Frame(master=self.window)
        self.problem_select_frame.pack()

        self.select_file_frame = tk.Frame(master=self.problem_select_frame)
        self.select_file_frame.grid(row=0, column=0)

        self.text_entry_frame = tk.Frame(master=self.problem_select_frame)
        self.text_entry_frame.grid(row=0, column=1)


        self.file_select_button = tk.Button(master=self.select_file_frame, text='Load SAT problem(s) from file', width=25, height=3)
        self.file_select_button.pack()

        self.selected_file_label=tk.Label(master=self.select_file_frame, text='No file selected')
        self.selected_file_label.pack()


        self.text_entry = tk.Text(master=self.text_entry_frame, width=40, height=5)
        self.text_entry.insert(tk.END, 'Enter CNF SAT problem(s) here')
        self.text_entry.pack()


        self.file_or_text_frame = tk.Frame(master=self.problem_select_frame)
        self.file_or_text_frame.grid(row=0, column=2)

        self.read_text = tk.IntVar()
        self.text_checkbutton = tk.Checkbutton(master=self.file_or_text_frame, text='Solve SAT problem(s) in text box',
                                               variable=self.read_text)
        self.text_checkbutton.pack(anchor=tk.W)

        self.read_file = tk.IntVar(value=1)
        self.file_checkbutton=tk.Checkbutton(master=self.file_or_text_frame, text='Solve SAT problem(s) in file',
                                             variable=self.read_file)
        self.file_checkbutton.pack(anchor=tk.W)


        self.options_frame = tk.Frame(master=self.window, height=20, width=500)
        self.options_frame.pack()

        self.quick_solve_button = \
            tk.Button(master=self.options_frame, text='Quick Solve\n(DIMACS file only)', width=25, height=3)
        self.quick_solve_button.grid(row=0, column=0)

        self.dpll_button = tk.Button(master=self.options_frame, text='DPLL', width=25, height=3)
        self.dpll_button.grid(row=0, column=1)

        self.cdcl_button = tk.Button(master=self.options_frame, text='CDCL', width=25, height=3)
        self.cdcl_button.grid(row=0, column=2)

        #self.breadth_first_button = tk.Button(master=self.options_frame, text='Breadth-first', width=25, height=3)
        #self.breadth_first_button.grid(row=0, column=2)

        #self.show_solutions_checkbox=tk.Checkbutton(master=self.options_frame, text='Show', variable=tk.IntVar())
        #self.num_solutions_entry = tk.Entry(master=self.options_frame, width=3)
        #self.solutions_label=tk.Label(master=self.options_frame, text='solution(s)')

        #self.num_solutions_entry.insert(0, '1')

        #self.show_solutions_checkbox.grid(row=0, column=3)
        #self.num_solutions_entry.grid(row=0, column=4)
        #self.solutions_label.grid(row=0, column=5)


        self.results_frame = tk.Frame(master=self.window,  height=20, width=20)
        self.results_frame.pack(fill=tk.BOTH, expand=True)

        self.results_label=tk.Label(master=self.results_frame, text='Select a solve method')
        self.results_label.pack()

        return


def main():
    gui = SATgui()
    return

if __name__ == '__main__':
    sys.exit(main())
