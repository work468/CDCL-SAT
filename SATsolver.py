#!/usr/bin/env python3

import sys
import copy
import subprocess
import queue
import threading
import time

class SATsolver:

    def __init__(self, debug_mode = False):
        self.debug_mode = debug_mode
        self.portfolio_methods = [self.zchaff, self.picosat, self.minisat]

    def debug(self, msg):
        if self.debug_mode:
            print(' DEBUG : ' + str(msg))

    def portfolio(self, dimacs_path):

        def sat_thread(method, dimacs_path, queue):
            result = method(dimacs_path)
            queue.put((result, method.__name__))

        q = queue.Queue()

        threads = [threading.Thread(target=sat_thread, args=(m, dimacs_path, q)) for m in self.portfolio_methods]

        for thread in threads:
            thread.start()

        results = []

        for result in range(len(self.portfolio_methods)):
            results.append(q.get())

        for result in results:
            print(str(result[0][1]) + ' ' + result[1])

        return results[0][0]

    def zchaff(self, dimacs_path):
        start = time.time()
        result = subprocess.check_output('./zchaff %s' % (dimacs_path), shell=True)
        end = time.time()
        result = result.split(b'\n')

        if 'Unsatisfiable' in str(result[4]):
            return []

        result = str(result[5].split(b' R')[0])[2:-1]

        result = result.split(' ')

        solution = []
        for assignment in result:
            if assignment[0] == '-':
                solution.append((assignment[1:], False))
            else:
                solution.append((assignment, True))

        return solution, end - start

    def picosat(self, dimacs_path):
        start = time.time()
        try:
            result = subprocess.check_output('picosat %s' % (dimacs_path), shell=True)
        except subprocess.CalledProcessError as ex:
            result = ex.output
        end = time.time()

        result = result.split(b'\n')

        if 'UNSATISFIABLE' in str(result[0]):
            return []

        assignments = []

        for line in result[1:-1]:
            assignments.append(str(line)[4:-1])

        assignments[len(assignments) - 1] = assignments[len(assignments) - 1][:-2]

        solution = []
        for line in assignments:
            values = line.split(' ')
            for value in values:
                if value[0] == '-':
                    solution.append((value[1:], False))
                else:
                    solution.append((value, True))

        return solution, end - start


    def minisat(self, dimacs_path):
        start = time.time()
        subprocess.run('minisat %s out' % (dimacs_path), shell=True, stdout=subprocess.DEVNULL)
        end = time.time()
        result = subprocess.check_output('cat out', shell=True)
        result = str(result.split(b'\n')[1])

        if result == 'UNSAT':
            return []

        result = result[2:-3].split(' ')

        solution = []
        for assignment in result:
            if assignment[0] == '-':
                solution.append((assignment[1:], False))
            else:
                solution.append((assignment, True))

        return solution, end - start


    def portfolio_solve(self, dimacs_path):
        return

    def remove_invalid_chars(self, input_text):
        #Returns the input stripped of any characters that shouldn't be in a CNF expression
        self.debug(input_text)
        allowed_characters = ['(', ')', '^', 'v', '¬']
        invalid_chars = 0;
        stripped_input = ''

        for char in input_text:
            if char.isalpha() or char in allowed_characters:
                stripped_input += char
            else:
                invalid_chars += 1

        return stripped_input


    def parseCNF(self, input_text):

        def is_literal(expr):
            return (len(expr) == 1 and expr.isalpha())

        def is_negation(expr):
            return (len(expr) == 2 and expr[0] == '¬' and is_literal(expr[1]))

        def is_disjunction(expr):
            if not 'v' in expr:
                return False

            for term in expr.split('v'):
                if not is_literal(term) and not is_negation(term):
                    return False

            return True

        def strip_brackets(expr):
            while (expr[0] == '(' and expr[-1] == ')'):
                expr = expr[1:-1]

            return expr

        def remove_double_negation(expr):
            terms = expr.split('v')

            for i in range(len(terms)):
                while len(terms[i]) > 2 and terms[i][:2] == '¬¬':
                    terms[i] = terms[i][2:]

            return 'v'.join(terms)

        stripped_input = self.remove_invalid_chars(input_text)

        expressions = stripped_input.split('^')
        while '' in expressions:
            expressions.remove('')
        for i in range(len(expressions)):
            expressions[i] = strip_brackets(expressions[i])
            expressions[i] = remove_double_negation(expressions[i])

        for expr in expressions:
            if not is_disjunction(expr) and not is_literal(expr) and not is_negation(expr):
                #print('Error: Input not in CNF')
                return []

        tuple_representation = []

        for expr in expressions:
            terms = []

            for term in expr.split('v'):
                if is_literal(term):
                    terms.append((term, True))
                else:
                    terms.append((term[-1], False))

            tuple_representation.append(terms)

        self.debug(tuple_representation)

        tuple_representation_setified = []

        for clause in tuple_representation:
            clause_set = []
            for literal in clause:
                if literal not in clause_set:
                    clause_set.append(literal)
            tuple_representation_setified.append(clause_set)

        return(tuple_representation_setified)

    def get_literals(self, cnf):
        literals = []
        for clause in cnf:
            for literal in clause:
                if literal[0] not in literals:
                    literals.append(literal[0])

        return literals

    def split_problem(self, cnf_problem, assignment):
        cnf_problem_split = []
        for clause in cnf_problem:
            split_clause = clause + []
            if assignment not in split_clause:
                if (assignment[0], not assignment[1]) in split_clause:
                    split_clause.remove((assignment[0], not assignment[1]))
                cnf_problem_split.append(split_clause)

        return cnf_problem_split

    def check_assignment(self, cnf_problem, assignment):
        cnf_problem_split = self.split_problem(cnf_problem, assignment)

        if len(cnf_problem_split) == 0:
            return [assignment]

        sub_solution = self.sat_dpll_parsed(cnf_problem_split)

        if sub_solution != []:
            return sorted([assignment] + sub_solution)

        return []

    def sat_dpll_parsed(self, cnf_problem):

        if [] in cnf_problem:
            return []

        clause = min(cnf_problem, key=len)

        for assignment in clause:
            cnf_problem_split = self.split_problem(cnf_problem, assignment)

            if len(cnf_problem_split) == 0:
                return [assignment]

            sub_solution = self.sat_dpll_parsed(cnf_problem_split)

            if sub_solution != []:
                return ([assignment] + sub_solution)

        return []



    def sat_dpll(self, cnf_problem, prev_solutions = []):
        self.debug('In SAT function')
        for clause in cnf_problem:
            if len(clause) == 0:
                self.debug('Problem: ' + str(cnf_problem) + ' is unsolvable')
                print('Unsolvable')
                return []

        literals = self.get_literals(cnf_problem)
        literals = sorted(literals)
        self.debug('Start of function - CNF Problem: ' + str(cnf_problem))
        self.debug('Literals in problem:' + str(literals))

        truth_values = [True, False]
        for literal in literals:
            for truth_value in truth_values:
                assignment = (literal, truth_value)
                print('Making assignment: ' + str(assignment))
                self.debug('CNF problem: ' + str(cnf_problem))
                self.debug('Assigning ' + str(literal) + ' to value ' + str(truth_value))
                cnf_problem_split = []
                for clause in cnf_problem:
                    split_clause = clause.copy()
                    if assignment not in split_clause:
                        self.debug(str(assignment) + ' not in ' + str(split_clause) + ', including in split problem')
                        if (assignment[0], not assignment[1]) in split_clause:
                            self.debug(str(assignment) + ' negated in ' + str(split_clause) + ', removing ' + str((assignment[0], not assignment[1])) + ' from clause')
                            split_clause.remove((assignment[0], not assignment[1]))
                        cnf_problem_split.append(split_clause)
                self.debug('Split problem: ' + str(cnf_problem_split))
                if len(cnf_problem_split) == 0:
                    if [assignment] in prev_solutions:
                        continue
                    return [assignment]

                sub_solution = self.sat_dpll(cnf_problem_split)
                if sub_solution != []:
                    sorted_solution = sorted([assignment] + sub_solution)
                    if sorted_solution in prev_solutions:
                        continue
                    return sorted_solution

        return []

    def sat_dpll_initial_solutions(self, cnf_problem):
        found_solutions = []
        current_solution = self.sat_dpll(cnf_problem)
        while(current_solution != []):
            found_solutions.append(current_solution)
            current_solution = self.sat_dpll(cnf_problem, found_solutions)
        return found_solutions

    def sat_dpll_all_solutions(self, cnf_problem):
        found_solutions = self.sat_dpll_initial_solutions(cnf_problem)
        self.debug('Initial solutions: ' + str(found_solutions))

        literals = self.get_literals(cnf_problem)

        def expand_solution(solution, literals):
            missing_literals = literals.copy()
            for assignment in solution:
                if assignment[0] in missing_literals:
                    missing_literals.remove(assignment[0])

            self.debug('Missing literals: ' + str(missing_literals))

            expanded_solutions = [solution]
            for literal in missing_literals:
                self.debug('Missing literal to expand on: ' + str(literal))
                new_expansions = []
                for sol in expanded_solutions:
                    self.debug('Solution we are expanding: ' + str(sol))
                    new_expansions.append(sol + [(literal, True)])
                    new_expansions.append(sol + [(literal, False)])
                expanded_solutions = new_expansions
                self.debug('Expanded solutions: ' + str(expanded_solutions))

            return expanded_solutions

        short_solutions = []

        for solution in found_solutions:
            if len(solution) < len(literals):
                short_solutions.append(solution)

        self.debug('Short solutions: ' + str(short_solutions))

        for to_be_removed in short_solutions:
            found_solutions.remove(to_be_removed)

        self.debug('Initial solutions - complete only: ' + str(found_solutions))

        for short_solution in short_solutions:
                self.debug('Short solution: ' + str(short_solution))
                expanded_solutions = expand_solution(short_solution.copy(), literals)
                self.debug('Short solution - original: ' + str(short_solution))
                self.debug('Expansions of short solution: ' + str(expanded_solutions))
                for expanded_solution in expanded_solutions:
                    expanded_sorted = sorted(expanded_solution)
                    if expanded_sorted not in found_solutions:
                        found_solutions.append(expanded_sorted)

        return sorted(found_solutions)

    def is_sat_solvable(self, problem_input):
        cnf_problem = self.parseCNF(problem_input)
        if cnf_problem == []:
            return False
        return self.sat_dpll(cnf_problem) != []

    def find_first_solution(self, problem_input):
        cnf_problem = self.parseCNF(problem_input)
        return self.sat_dpll(cnf_problem)

    def find_initial_solutions(self, problem_input):
        cnf_problem = self.parseCNF(problem_input)
        if cnf_problem == []:
            return []
        return self.sat_dpll_initial_solutions(cnf_problem)

    def find_all_solutions(self, problem_input):
        # TODO: This needs finishing if we decide it has value. Currently returns many solutions but not necessarily all
        cnf_problem = self.parseCNF(problem_input)
        if cnf_problem == []:
            return []
        return self.sat_dpll_all_solutions(cnf_problem)

    def get_all_combos(self, literals, num_literals):
        combos = []
        if num_literals == 1:
            for literal in literals:
                combos.append([literal])
            return combos

        for literal in literals:
            other_literals = literals.copy()
            other_literals.remove(literal)
            combos_subset= self.get_all_combos(other_literals, num_literals-1)
            for combo in combos_subset:
                self.debug('Sub combo: ' + str(combo))
                combo += literal
                self.debug('Full combo: ' + str(combo))
                combo = sorted(combo)
                self.debug('Sorted combo: ' + str(combo))
                if combo not in combos:
                    combos.append(combo)
                self.debug('Current combos: ' + str(combos))

        combos = sorted(combos)
        self.debug('All combos (sorted): ' + str(combos))
        return combos

    def get_all_assignments(self, literal_combos):
        assignments = []
        for combo in literal_combos:
            self.debug('Combo to find all assignments: ' + str(combo))
            if len(combo) == 1:
                assignments.append([(combo[0], True)])
                assignments.append([(combo[0], False)])
            else:
                sub_assignments = self.get_all_assignments([combo[1:]])
                for sub_assignment in sub_assignments:
                    assignments.append(sub_assignment.copy() + [(combo[0], True)])
                    assignments.append(sub_assignment.copy() + [(combo[0], False)])
                self.debug('Added sub assignments: ' + str(sub_assignments))

        self.debug('Found assignments: ' + str(assignments))

        assignments_set = []
        for assignment in assignments:
            assignment = sorted(assignment)
            if assignment not in assignments_set:
                assignments_set.append(assignment)

        return assignments_set

    def assignment_satisfies(self, cnf_problem, assignment):
        self.debug('Checking if ' + str(assignment) + ' satisfies ' + str(cnf_problem))
        problem_remaining = copy.deepcopy(cnf_problem)
        for clause in problem_remaining:
            if not any(literal_value in clause for literal_value in assignment):
                #print(clause)
                #print(assignment)
                return False

        return True

    def sat_breadth_first_parsed(self, cnf_problem):
        self.debug('Breadth first CNF parsed problem: ' + str(cnf_problem))
        literals = self.get_literals(cnf_problem)
        for num_literals in range(1, len(literals) + 1):
            literal_combos = self.get_all_combos(literals, num_literals)
            self.debug('Found these combos for ' + str(num_literals) + ' literals: ' + str(literal_combos))
            assignments = self.get_all_assignments(literal_combos)
            self.debug('Generated these assignments: ' + str(assignments))
            for assignment in assignments:
                if self.assignment_satisfies(cnf_problem, assignment):
                    return assignment

        return []

    def breadth_first_sat_parsed(self, cnf_problem):
        self.debug('Breadth first CNF parsed problem: ' + str(cnf_problem))
        literals_list = self.get_literals(cnf_problem)
        assignments = []
        for literal in literals_list:
            if self.assignment_satisfies(cnf_problem, [(literal, True)]):
                return sorted([(literal, True)])
            if self.assignment_satisfies(cnf_problem, [(literal, False)]):
                return sorted([(literal, False)])
            assignments.append([
                                [
                                    literal
                                ],
                                [
                                    [(literal, True)],
                                    [(literal, False)]
                                ]
                               ])

        #import pdb; pdb.set_trace()
        for num_literals in range(2, len(literals_list) + 1):
            expanded_assignments = []
            for literal in literals_list:
                for literals in assignments:
                    if literal not in literals[0]:
                        #print(str(literals[0]))
                        #print(str(literal))
                        #print(str(literals))
                        #import pdb;
                        #pdb.set_trace()
                        literal_added = sorted(literals[0] + [literal])
                        if not any(literals_list[0] == literal_added for literals_list in expanded_assignments):
                            expanded_literals = [literal_added, []]
                            for assignment in literals[1]:
                                new_assignment = assignment + [(literal, True)]
                                if self.assignment_satisfies(cnf_problem, new_assignment):
                                    return sorted(new_assignment)
                                expanded_literals[1].append(new_assignment)

                                new_assignment = assignment + [(literal, False)]
                                if self.assignment_satisfies(cnf_problem, new_assignment):
                                    return sorted(new_assignment)
                                expanded_literals[1].append(new_assignment)

                            expanded_assignments.append(expanded_literals)

            self.debug('Assignments: ' + str(assignments))
            self.debug('Expanded assignments: ' + str(expanded_assignments))
            assignments = expanded_assignments

        return []


    def sat_breadth_first(self, problem_input):
        cnf_problem = self.parseCNF(problem_input)
        return self.sat_breadth_first_parsed(cnf_problem)

    def parse_dimacs(self, input):
        # TODO: Handle bad formatting
        parsed = []
        for line in input:
            if len(line) == 0 or line[0].isalpha():
                continue

            clause = []
            terms = line.split(' ')
            terms = terms[:-1]

            for term in terms:
                if len(term) == 0:
                    continue
                if term[0] == '-':
                    clause.append((term[1:], False))
                else:
                    clause.append((term, True))

            parsed.append(clause)

        return parsed

    def cdcl_parsed(self, file_parsed):
        cnf_problem = copy.deepcopy(file_parsed)
        self.debug('Problem passed to cdcl:\n' + str(cnf_problem))

        def nonify(cnf_problem):
            for clause in range(len(cnf_problem)):
                for term in range(len(cnf_problem[clause])):
                    cnf_problem[clause][term] = (cnf_problem[clause][term], None)

            return cnf_problem

        def clause_satisfied(clause):
            for term in clause:
                if term[1] == True:
                    return True

            return False

        def problem_satisfied(cnf_problem):
            for clause in cnf_problem:
                if not clause_satisfied(clause):
                    return False

            return True

        # TODO: MAYBE look at satisfying shorter clauses first
        def choose_assignment(cnf_problem):
            for clause in cnf_problem:
                if clause_satisfied(clause):
                    continue
                for term in clause:
                    if term[1] == None:
                        return term[0]

        def choose_assignment_from_unassigned(cnf_problem):
            for clause in cnf_problem:
                for term in clause:
                    if term[1] == None:
                        return term[0]

        def apply_assignment(cnf_problem, assignment):
            for clause in range(len(cnf_problem)):
                for term in range(len(cnf_problem[clause])):
                    if cnf_problem[clause][term][0][0] == assignment[0]:
                        if cnf_problem[clause][term][0][1] == assignment[1]:
                            cnf_problem[clause][term] = (cnf_problem[clause][term][0], True)
                        else:
                            cnf_problem[clause][term] = (cnf_problem[clause][term][0], False)

            return cnf_problem

        def unapply_assignments(cnf_problem, assignments):
            for assignment in assignments:
                for clause in range(len(cnf_problem)):
                    for term in range(len(cnf_problem[clause])):
                        if cnf_problem[clause][term][0][0] == assignment[0]:
                            cnf_problem[clause][term] = (cnf_problem[clause][term][0], None)

            return cnf_problem

        def unit_prop(cnf_problem):
            new_assignments = []
            found_assignments = []
            for clause in cnf_problem:
                none = []
                true = []
                false = []
                for literal in clause:
                    if literal[1] == None:
                        none.append(literal[0])
                        if len(none) > 1:
                            break;
                    if literal[1] == True:
                        true.append(literal[0])
                        break;
                    if literal[1] == False:
                        false.append((literal[0][0], not literal[0][1]))

                if len(true) == 0 and len(none) == 1:
                    if none[0] not in found_assignments:
                        new_assignments.append((none[0], false))
                        found_assignments.append(none[0])

            return new_assignments

        def check_conflicts(implication_graph, new_assignments):
            #TODO: I'm pretty sure this code was written for an impossible case - try removing it at some point
            for assignment in new_assignments:
                for previous in implication_graph:
                    if (assignment[0][0], assignment[0][1]) == (previous[0][0], not previous[0][1]):
                        return [assignment]

            for assignment in new_assignments:
                for assignment2 in new_assignments:
                    if assignment[0] == (assignment2[0][0], not assignment2[0][1]):
                        return [assignment, assignment2]

            return None

        def level_0_conflict(cnf_problem, implication_graph):
            applied_assignments = []
            new_assignments = unit_prop(cnf_problem)
            while(not new_assignments == []):
                if not check_conflicts(implication_graph, new_assignments) == None:
                    return True, applied_assignments

                for new_assignment in new_assignments:
                    self.debug('Applying level 0 unit prop: ' + str(new_assignment))
                    cnf_problem = apply_assignment(cnf_problem, new_assignment[0])
                    applied_assignments.append(new_assignment[0])
                    implication_graph.append(new_assignment)

                new_assignments = unit_prop(cnf_problem)

            return False, applied_assignments

        def all_variables_assigned(cnf_problem):
            for clause in cnf_problem:
                for term in clause:
                    if term[1] == None:
                        return False

            return True

        def get_unsat_causes(conflicts, implication_graph):
            causes = []
            for conflict in conflicts:
                for node in implication_graph:
                    if node[0] == (conflict[0][0], not conflict[0][1]):
                        for cause in node[1]:
                            if cause not in causes:
                                causes.append(cause)

            return causes

        def get_conflict_causes(conflicts, implication_graph):
            causes = conflicts[0][1]
            if len(conflicts) == 1:
                for var in implication_graph:
                    if var[0] == (conflicts[0][0], not conflicts[0][1]):
                        for cause in var[1]:
                            if cause not in causes:
                                causes.append(cause)
            if len(conflicts) == 2:
                for cause in conflicts[1][1]:
                    if cause not in causes:
                        causes.append(cause)

            return causes

        def check_level_conflicts(decisions, causes, implication_graph):
            for assignment in decisions:
                if assignment in causes:
                    causes.remove(assignment)
                for node in implication_graph:
                    if node[0] == assignment:
                        i_g_to_be_removed.append(node)

            return causes, i_g_to_be_removed

        def move_up_level(cnf_problem, decisions, decision_level, implication_graph, i_g_to_be_removed):
            cnf_problem = unapply_assignments(cnf_problem, decisions[decision_level])

            for node in i_g_to_be_removed:
                implication_graph.remove(node)

            decisions.remove(decisions[-1])
            decision_level -= 1

            return cnf_problem, decisions, decision_level, implication_graph

        def get_unsatisfiable(cnf_problem):
            for clause in cnf_problem:
                if all(literal[1] == False for literal in clause):
                    return clause

            return None

        cnf_problem = nonify(cnf_problem)

        implication_graph = []
        decisions = []
        decision_level = -1

        level_0_propogations = []

        while(not all_variables_assigned(cnf_problem)):
            if decision_level == -1:
                self.debug('\nDecisions:\n' + str(decisions) +
                           '\nDecision level:\n' + str(decision_level) +
                           '\nImplication graph:\n' + str(implication_graph) +
                           '\nState of problem:\n' + str(cnf_problem))

                conflicts, level_0_propogations = level_0_conflict(cnf_problem, implication_graph)

                if conflicts:
                    self.debug('decision level == -1 and level_0_conflict(cnf_problem, implication_graph')
                    return []

                self.debug('No level -1 errors found')

            assignment = choose_assignment(cnf_problem)
            if assignment == None:
                assignment = choose_assignment_from_unassigned(cnf_problem)
                if assignment == None:
                    break
            cnf_problem = apply_assignment(cnf_problem, assignment)
            decision_level += 1
            decisions.append([assignment])
            implication_graph.append((assignment, []))

            self.debug('Chose assignment: ' + str(assignment))
            self.debug('\nDecisions:\n' + str(decisions) +
                       '\nDecision level:\n' + str(decision_level) +
                       '\nState of problem:\n' + str(cnf_problem) +
                       '\nImplication graph:\n' + str(implication_graph))

            new_assignments = unit_prop(cnf_problem)
            unsatisfiable = None
            while ((not new_assignments == [] and not decision_level == -1) or not unsatisfiable == None):
                self.debug('New assignments: ' + str(new_assignments))
                if unsatisfiable == None:
                    conflicts = check_conflicts(implication_graph, new_assignments)
                    self.debug('check_conflicts returned: ' + str(conflicts))
                else:
                    conflicts = unsatisfiable
                    self.debug('Unsatisfiable clause: ' + str(unsatisfiable))

                if conflicts == None:
                    for new_assignment in new_assignments:
                        self.debug('Applying assignment: ' + str(new_assignment[0]))
                        cnf_problem = apply_assignment(cnf_problem, new_assignment[0])
                        implication_graph.append(new_assignment)
                        decisions[decision_level].append(new_assignment[0])

                        self.debug('\nDecisions:\n' + str(decisions) +
                                   '\nImplication graph:\n' + str(implication_graph) +
                                   '\nState of problem:\n' + str(cnf_problem))

                    new_assignments = unit_prop(cnf_problem)
                    unsatisfiable = get_unsatisfiable(cnf_problem)
                    self.debug('New assignments after last application: ' + str(new_assignments))
                else:
                    if unsatisfiable == None:
                        causes = get_conflict_causes(conflicts, implication_graph)
                    else:
                        causes = get_unsat_causes(conflicts, implication_graph)

                    self.debug('Conflicts found. Causes: ' + str(causes))

                    new_clause = []
                    for cause in causes:
                        if (cause[0], not cause[1]) not in new_clause:
                            new_clause.append((cause[0], not cause[1]))

                    self.debug('New clause to add:' + str(new_clause))

                    new_clause = nonify([new_clause])[0]
                    cnf_problem.append(new_clause)

                    self.debug('cnf_problem after clause added:' + str(cnf_problem))
                    self.debug('Implication graph before backtracking: ' + str(implication_graph))

                    i_g_to_be_removed = []

                    causes, i_g_to_be_removed = check_level_conflicts(decisions[decision_level],
                                                                      causes, implication_graph)

                    self.debug('Before moving up FIRST level')
                    self.debug('\nDecisions:\n' + str(decisions) +
                               '\nDecision level:' + str(decision_level) +
                               '\nImplication graph:\n' + str(implication_graph) +
                               '\nCauses:\n' + str(causes) +
                               '\nState of problem:\n' + str(cnf_problem))

                    cnf_problem, decisions, decision_level, implication_graph = \
                        move_up_level(cnf_problem, decisions, decision_level, implication_graph, i_g_to_be_removed)

                    self.debug('After moving up FIRST level')
                    self.debug('\nDecisions:\n' + str(decisions) +
                               '\nDecision level:' + str(decision_level) +
                               '\nImplication graph:\n' + str(implication_graph) +
                               '\nCauses:\n' + str(causes) +
                               '\nState of problem:\n' + str(cnf_problem))

                    while not causes == []:
                        i_g_to_be_removed = []

                        self.debug('Causes remaining ' + str(causes))

                        if decision_level == -1:
                            causes = []
                            cnf_problem = unapply_assignments(cnf_problem, level_0_propogations)
                        else:
                            causes, i_g_to_be_removed = check_level_conflicts(decisions[decision_level],
                                                                              causes, implication_graph)

                        self.debug('\nDecision level: ' + str(decision_level) +
                                   '\ni_g_to_be_removed: ' + str(implication_graph) +
                                   '\ncauses after looking at current decision level: ' + str(causes))

                        if not causes == []:
                            self.debug('causes not yet empty, unapplying current decision level to cnf_problem ' +
                                       ' and implication graph')

                            cnf_problem, decisions, decision_level, implication_graph = \
                                move_up_level(cnf_problem, decisions, decision_level, implication_graph,
                                              i_g_to_be_removed)

                            self.debug('After moving up a decision level')
                            self.debug('\nDecisions:\n' + str(decisions) +
                                       '\nDecision level:' + str(decision_level) +
                                       '\nImplication graph:\n' + str(implication_graph) +
                                       '\nState of problem:\n' + str(cnf_problem))

                    self.debug('Re-applying all previous assignments for the new clause')
                    self.debug('Problem before: ' + str(cnf_problem))
                    for level in decisions:
                        for assignment in level:
                            cnf_problem = apply_assignment(cnf_problem, assignment)

                    self.debug('Problem after: ' + str(cnf_problem))

                    new_assignments = unit_prop(cnf_problem)
                    unsatisfiable = get_unsatisfiable(cnf_problem)


        self.debug('Exited main while loop, building solution')
        self.debug('Decisions:\n' + str(decisions))
        solution = []
        for level in decisions:
            for assignment in level:
                solution.append(assignment)

        self.debug('level_0_propogations:\n' + str(level_0_propogations))

        solution += level_0_propogations

        self.debug('solution:\n' + str(solution))

        return solution

def main(input_text, debug_arg = False):
    return
    #global debug_mode
    #debug_mode = debug_arg
    #if parseCNF(input_text) == []:
        #print('!!!INPUT NOT IN CNF!!!')
        #sys.exit()
    #literals = get_literals(parseCNF(input_text))
    #debug('Literals: ' + str(literals))
    #for x in range(1, len(literals) + 1):
        #debug('Size of each combo: ' + str(x))
        #all_combos = get_all_combos(literals, x)
        #for combo in all_combos:
            #debug('Combo: ' + str(combo))
        #print('Size of each combo: ' + str(x))
        #print('get_all_combos: ')
        #for combo in all_combos: 
            #print(str(combo))
        #all_assignments = get_all_assignments(all_combos)
        #print('get_all_assignments: ')
        #for assignment in all_assignments:
            #print(str(assignment))
    #print(sat_breadth_first(input_text))


    #print('is_sat_solvable output: ' + str(is_sat_solvable(input_text)))
    #print('find_first_solution output: ' + str(find_first_solution(input_text)))
    #print('find_initial_solutions output: ' + str(find_initial_solutions(input_text)))
    #print('find_all_solutions output:')
    #for sol in find_all_solutions(input_text):
        #print(str(sol))
    



if __name__ == '__main__':
    sys.exit(main(sys.argv[1]))
